# Weblog Helper

## Using Ruby version 2.2.1p85 (2015-02-26 revision 49769) [x86_64-linux]

WeblogHelper will search through a http access log file 
with a specified IP address or IP CIDR range and return 
all log entries that correspond with the given IP address
or CIDR range.

Requires the 'netaddr' gem.

To run the program, use the following syntax:

**./weblog_helper.rb --ip <ip-address>**
