#!/usr/bin/env ruby

# Requires the 'test-unit' gem

require "test/unit"
require_relative '../weblog_helper'

class TestWeblogHelper < Test::Unit::TestCase

	def test_parse_ip_address
		assert_kind_of(ArgumentError, WeblogHelper.new)
		assert_equal("Available log entries returned", WeblogHelper.new("127.0.0.1").parse_ip_address)
		assert_equal("Available log entries returned", WeblogHelper.new("127.0.0.1/30").parse_ip_address)
		assert_equal("Invalid IP address or range. Please try again using a valid IP address or range.", WeblogHelper.new("987.65.43.21").parse_ip_address)
		assert_equal("Invalid IP address or range. Please try again using a valid IP address or range.", WeblogHelper.new("987.65.43.21/30").parse_ip_address)
		assert_equal("Invalid IP address or range. Please try again using a valid IP address or range.", WeblogHelper.new("Not an IP address").parse_ip_address)
	end

	def test_check_log
		assert_equal("Invalid address type. Cannot check log", WeblogHelper.new("127.0.0.1").check_log("127.0.0.1", 'Apple'))
		assert_equal("Checking log file", WeblogHelper.new("127.0.0.1").check_log("127.0.0.1", 'ip'))
		assert_equal("Checking log file", WeblogHelper.new("127.0.0.1/30").check_log(NetAddr::CIDR.create("127.0.0.1/30").enumerate, 'cidr'))
		assert_kind_of(NoMethodError, WeblogHelper.new("127.0.0.1").check_log("127.0.0.1", 'cidr'))

	end

	def test_open_log_file
		assert_equal("Log file not found", WeblogHelper.new("127.0.0.1").open_log_file("127.0.0.1", "./path/to/file"))
		assert_equal("Log file not found", WeblogHelper.new("127.0.0.1").open_log_file("127.0.0.1", nil))
		assert_equal("Log file opened successfully", WeblogHelper.new("127.0.0.1").open_log_file("127.0.0.1", "./log/public_access.log.txt"))
	end

end

