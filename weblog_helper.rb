#!/usr/bin/env ruby

=begin

WeblogHelper will search through a http access log file 
with a specified IP address or IP CIDR range and return 
all log entries that correspond with the given IP address
or CIDR range.

Requires the 'netaddr' gem.

Author: 					Tom O'Brien
Date: 						November 5, 2015
Ruby version: 		2.2.1p85

=end

require 'optparse'
require 'resolv'
require 'netaddr'

class WeblogHelper

	def initialize(ip_addr)
		@ip_addr = ip_addr
	end

	# Determine if @ip_addr is a single IP, a CIDR range, or neither
	def parse_ip_address
		begin
			if @ip_addr =~ Resolv::IPv4::Regex		
				# Check logfile with @ip_addr
				check_log(@ip_addr, 'ip')
				"Available log entries returned"
			elsif @ip_addr =~ /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$/
				# Check logfile with array returned from NetAddr::CIDR
				check_log(NetAddr::CIDR.create(@ip_addr).enumerate, 'cidr')
				"Available log entries returned"
			else
				p "Invalid IP address or range. Please try again using a valid IP address or range."
			end
		rescue OptionParser::MissingArgument
			p "Please specify an IP address or range with the --ip switch"
		rescue Exception => msg  
		  p msg 
		end 
	end

	def check_log(ip_addr, address_type)
		log_file = "./log/public_access.log.txt"

		if address_type == 'ip'
			begin
				open_log_file(ip_addr, log_file)
				"Checking log file"
			rescue Exception => msg  
			  p msg 
			end
		elsif address_type == 'cidr'
			begin
				ip_addr.each do |ip|
					open_log_file(ip, log_file)
				end
				"Checking log file"
			rescue Exception => msg  
			  p msg 
			end
		else
			p "Invalid address type. Cannot check log"
		end
	end

	def open_log_file(ip_addr, log_file)
		if File.exist?(log_file)
			File.open(log_file) do |f| 
				f.each_line do |line| 
					if /#{ip_addr}/.match(line) 
						p line
					end
				end
			end
			"Log file opened successfully"
		else
			p "Log file not found"
			exit
		end
	end

end

options = {}
OptionParser.new do |opt|
  opt.on('--ip IP_ADDRESS') do |option| 
  	options[:ip_address] = option 
  end
end.parse!

weblog_helper = WeblogHelper.new(options[:ip_address])

weblog_helper.parse_ip_address